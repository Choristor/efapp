﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class GetAveragesResult
    {
		public string Job { get; set; }
		public decimal? Avg { get; set; }

		public override bool Equals(object obj) {
			if(obj is GetAveragesResult) {
				GetAveragesResult other = obj as GetAveragesResult;
				return this.Job == other.Job && this.Avg == other.Avg;
			}
			return false;
		}

		public override int GetHashCode() {
			return 0;
		}
		public override string ToString() {
			return Job + ": " + Avg;
		}
	}
}
