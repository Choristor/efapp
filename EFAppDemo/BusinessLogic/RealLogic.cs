﻿using Repository;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.EmpRepos;
using Repository.DeptRepos;

namespace BusinessLogic {
	public class RealLogic:ILogic {
		MyRepository Repo;
		

		public RealLogic(MyRepository newRepo) {
			Repo = newRepo;
		}

		public IQueryable<GetAveragesResult> GetAverages() {
			
			var q = from akt in Repo.empRepo.GetAll()
					group akt by akt.JOB into g
					select new GetAveragesResult() {
						Job = g.Key,
						Avg = g.Average(x => x.SAL)
					};
			return q;
		}

		public IQueryable<DEPT> GetDepartments() {
			return Repo.deptRepo.GetAll();
		}

		public IQueryable<EMP> GetWorkers() {
			return Repo.empRepo.GetAll();
		}


		public RealLogic() {
			EmpDeptEntities ED = new EmpDeptEntities();
			EmpEFRepository er = new EmpEFRepository(ED);
			DeptEFRepository dr = new DeptEFRepository(ED);
			Repo = new MyRepository(er, dr);
		}

		public EMP GetOneWorker(int id) {
			return Repo.empRepo.GetById(id);
		}

		public void AddDept(DEPT newdept) {
			Repo.deptRepo.Insert(newdept);
		}
		public void DelDept(int id) {
			Repo.deptRepo.Delete(id);
		}

		public void ModifyDept(int id, string name, string loc) {
			Repo.deptRepo.Modify(id, name, loc);
		}

		public DEPT GetDept(int id) {
			return Repo.deptRepo.GetById(id);
		}

		public int GetNextDeptno() {
			return Repo.deptRepo.GetAll().Max(x => (int)x.DEPTNO) + 10;
		}
	}
}
