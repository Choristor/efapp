﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.EmpRepos;
using Repository.DeptRepos;
using Repository;
using BusinessLogic;

namespace EFApp {
	class Program {
		static void Main(string[] args) {
			EmpDeptEntities ED = new EmpDeptEntities();
			//foreach(var akt in ED.EMP) {
			//	Console.WriteLine(akt.ENAME);
			//}
			////IQueryable = Delayed, Optimized
			//var q1 = from akt in ED.EMP
			//		 group akt by akt.JOB into g
			//		 select new { JOB = g.Key, NUM = g.Count(), AVG = g.Average(x => x.SAL + (x.COMM ?? 0)) };
			//foreach(var akt in q1) {
			//	Console.WriteLine(akt);
			//}
			//Console.WriteLine(q1);
			//Console.ReadLine();
			//var q2 = from akt in ED.EMP
			//		 join data in q1 on akt.JOB equals data.JOB
			//		 select new { akt.ENAME, akt.JOB, data.AVG,akt.DEPT.DNAME };
			//foreach(var akt in q1) {
			//	Console.WriteLine(akt);
			//}
			//Console.WriteLine(q2);
			IEmpRepository er = new EmpEFRepository(ED);
			IDeptRepository dr = new DeptListRepository();
			MyRepository repo = new MyRepository(er, dr);

			ILogic BL = new RealLogic(repo);
			foreach(var akt in BL.GetAverages()){
				Console.WriteLine(akt);
			}

			Console.ReadLine();
		}
	}
}
