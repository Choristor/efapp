﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.EmpRepos {
	public class EmpEFRepository : EFRepository<EMP>,IEmpRepository {
		public EmpEFRepository(DbContext newctx) : base(newctx) {
		}

		public override EMP GetById(int id) {
			return Get(akt => akt.EMPNO == id).SingleOrDefault();
		}
	}
}
