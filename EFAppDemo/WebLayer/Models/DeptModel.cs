﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebLayer.Models {
	public class DeptModel {
		[Display(Name = "Department ID")]
		[Required]
		public int deptno { get; set; }
		[Display(Name = "Department Name")]
		[Required]
		[StringLength(14,MinimumLength = 5)]
		public string dname { get; set; }
		[Display(Name = "Department Location")]
		[Required]
		[StringLength(20,MinimumLength = 5)]
		public string loc { get; set; }

	}
}