﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos {
	abstract public class ListRepository<TEntity> : IRepository<TEntity> where TEntity : class {
		protected List<TEntity> list;
		public ListRepository(params TEntity[] entries) {
			list = new List<TEntity>();
			list.AddRange(entries);
		}
		public abstract TEntity GetById(int id);
		public void Delete(int id) {
			TEntity oldidentity = GetById(id);
			if(oldidentity == null)
				throw new ArgumentException("NO DATA");
			Delete(oldidentity);
		}
		//DRY = Dont Repeat Yourself
		//DDD =Domain Driven Design

		public void Delete(TEntity oldentity) {
			list.Remove(oldentity);
		}

		public void Dispose() {
			list.Clear();
			list = null;
		}

		public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition) {
			return list.Where(condition.Compile()).AsQueryable();
		}

		public IQueryable<TEntity> GetAll() {
			return list.AsQueryable();
		}


		public void Insert(TEntity newentity) {
			list.Add(newentity);
		}
	}
}
