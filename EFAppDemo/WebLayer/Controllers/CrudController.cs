﻿using AutoMapper;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebLayer.Models;

namespace WebLayer.Controllers
{
    public class CrudController : Controller
    {
		IMapper mapper;
		ILogic logic;
		CrudModel model;
		protected override void OnActionExecuting(ActionExecutingContext filterContext) {
			mapper = DTO.AutoMapperConfig.GetMapper();
			logic = new RealLogic();
			model = new CrudModel();
			model.EditObject = new DeptModel();
			var dblist = logic.GetDepartments();
			model.List = mapper.Map<IQueryable<Data.DEPT>, List<Models.DeptModel>>(dblist);
			base.OnActionExecuting(filterContext);
		}
		private DeptModel getDeptModel(int id) {
			Data.DEPT d = logic.GetDept(id);
			return mapper.Map<Data.DEPT,Models.DeptModel > (d);
		}
		// GET: Crud
		public ActionResult Index()
        {
			
			ViewData["TargetAction"] = "AddNew";
            return View("CrudIndex",model);
        }

		public ActionResult Details(int id) {
			return View("CrudDatasheet", getDeptModel(id));
		}
		//Fat Model, Skinny Controller

		public ActionResult Edit(int id) {
			ViewData["TargetAction"] = "Edit";
			model.EditObject = getDeptModel(id);
			return View("CrudIndex", model);
		}

		[HttpPost]
		public ActionResult Edit(DeptModel model) {
			if(ModelState.IsValid && model != null) {
				logic.ModifyDept(model.deptno, model.dname, model.loc);
				TempData["result"] = "Edit OK";
			} else {
				TempData["result"] = "Edit FAIL";
			}
			return RedirectToAction("Index");
		}
		[HttpPost]
		public ActionResult AddNew(DeptModel model) {
			if(ModelState.IsValid && model != null) {
				Data.DEPT d = mapper.Map<Models.DeptModel, Data.DEPT>(model);
				d.DEPTNO = logic.GetNextDeptno();
				logic.AddDept(d);
				TempData["result"] = "Add OK";
			} else {
				TempData["result"] = "Add FAIL";
			}
			return RedirectToAction("Index");
		}

		
		public ActionResult Remove(int id) {
			try {
				logic.DelDept(id);
				TempData["result"] = "Del OK";
			} catch(DbUpdateException) {
				TempData["result"] = "Del FAIL";
			}
			 catch(ArgumentException) {
				TempData["result"] = "Del Fail";
			}
			return RedirectToAction("Index");
		}

	}
}