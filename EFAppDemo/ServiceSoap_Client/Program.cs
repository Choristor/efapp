﻿using ServiceSoap_Client.EmpService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceSoap_Client {
	class Program {
		static void Main(string[] args) {
			EmpServiceClient client = new EmpServiceClient();
			foreach(var item in client.GetWorkerAverages()) {
				Console.WriteLine("{0}: {1}",item.Job,item.AvgSal);
			}
			var worker = client.GetWorkerById(7788);
			Console.WriteLine("{0}\t{1}\t{2}", worker.Empno,worker.Ename,worker.Job);
			Console.ReadLine();
			
		}
	}
}
