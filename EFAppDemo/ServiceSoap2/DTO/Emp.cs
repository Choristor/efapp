﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceSoap2.DTO {
	[DataContract]
	public class Emp {
		[DataMember]
		public int Empno { get; set; }
		[DataMember]
		public string Ename { get; set; }
		[DataMember]
		public string Job { get; set; }
		

	}
}