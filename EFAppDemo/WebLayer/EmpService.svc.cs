//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using Data;
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace WebLayer
{
	[ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class EmpService : EntityFrameworkDataService<EmpDeptEntities>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;

			config.SetEntitySetAccessRule("*", EntitySetRights.All);
			config.SetServiceOperationAccessRule("EmpByName", ServiceOperationRights.All);
			config.UseVerboseErrors = true;
        }

		[WebGet]
		public IQueryable<EMP> EmpByName(string name) {
			if(name == null)
				return null;
			return from akt in CurrentDataSource.EMP
				   where akt.ENAME.ToLower().Contains(name.ToLower())
				   select akt;
		}

    }
}
