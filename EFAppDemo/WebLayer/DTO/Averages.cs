﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WebLayer.DTO {
	
	public class Averages {
		
		public string Job { get; set; }
		
		public double AvgSal { get; set; }
	}
}