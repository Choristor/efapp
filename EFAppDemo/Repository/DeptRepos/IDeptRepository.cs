﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DeptRepos {
	public interface IDeptRepository : IRepository<DEPT> {
		void Modify(int id, string newname, string newloc);
	}
}
