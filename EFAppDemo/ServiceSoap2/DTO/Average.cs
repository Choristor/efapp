﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceSoap2.DTO {
	[DataContract]
	public class Average {
		[DataMember]
		public string Job { get; set; }
		[DataMember]
		public double AvgSal { get; set; }
	}
}