﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.DeptRepos {
	public class DeptListRepository :ListRepository<DEPT>,IDeptRepository {
		public DeptListRepository() : base(
			new DEPT() { DEPTNO = 10, DNAME = "ACCOUNTINGX", LOC = "NEW YORK" },
			new DEPT() { DEPTNO = 20, DNAME = "RESEARCHX", LOC = "DALLAS" },
			new DEPT() { DEPTNO = 30, DNAME = "SALESX", LOC = "cHICAGO" },
			new DEPT() { DEPTNO = 40, DNAME = "OPERATIONSX", LOC = "BOSTON" }
			) 
		{

		}

		public override DEPT GetById(int id) {
			return Get(akt => akt.DEPTNO == id).SingleOrDefault();
		}

		public void Modify(int id, string newname, string newloc) {
			DEPT akt = GetById(id);
			if(akt == null)
				throw new ArgumentException("NO DATA");
			if(newname != null)
				akt.DNAME = newname;
			if(newloc != null)
				akt.LOC = newloc;
			list[id] = akt;
			
		}
	}
}
