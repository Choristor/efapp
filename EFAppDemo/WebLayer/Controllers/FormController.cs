﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebLayer.Models;

namespace WebLayer.Controllers
{
    public class FormController : Controller
    {
        // GET: Form
        public ActionResult Index()
        {
            return View();
        }
		public ActionResult ProcessOne() {
			ViewBag.ProcessedOutput = String.Format("PROCESS ONE: uname={0},upass={1}", Request["userName"], Request["userPass"]);
			return View("Index");

		}
		public ActionResult ProcessTwo(string userName,string userPass) {
			ViewBag.ProcessedOutput = String.Format("PROCESS TWO: uname={0},upass={1}", Request["userName"], Request["userPass"]);
			return View("Index");
		}

		public ActionResult Ajax() {
			return View();
		}
		[HttpPost]
		public ActionResult Calculate(double amount,double rate, int year) {
			double total = (amount * rate * year/100);
			StringBuilder SB = new StringBuilder();
			SB.AppendFormat("<strong>Amount:</strong> {0}\n", amount);
			SB.AppendFormat("<strong>Rate:</strong> {0}\n", rate);
			SB.AppendFormat("<strong>Years:</strong> {0}\n", year);
			SB.AppendFormat("<strong>Total interest:</strong> {0}\n", total);
			ViewBag.FormResults = SB.ToString();
			//return View("Ajax");
			return Content(SB.ToString()); //With AjaxForm

		}
		public ActionResult AjaxModel() {
			CalculationModel model = new CalculationModel();
			model.Amount = 10000;
			model.Rate = 4.2;
			model.Years = 10;
			return View(model);
		}
		//Cross site request Forgery
		[HttpPost]
		[ValidateAntiForgeryToken]//cross site forgery ellenörzés
		public ActionResult CalculateWithModel(CalculationModel model) {
			if(ModelState.IsValid) {
				double total = (model.Amount * model.Rate * model.Years / 100);
				StringBuilder SB = new StringBuilder();
				SB.AppendFormat("<strong>Amount:</strong> {0}\n", model.Amount);
				SB.AppendFormat("<strong>Rate:</strong> {0}\n", model.Rate);
				SB.AppendFormat("<strong>Years:</strong> {0}\n", model.Years);
				SB.AppendFormat("<strong>Total interest:</strong> {0}\n", total);
				ViewBag.FormResults = SB.ToString();
				//return View("Ajax");
				return Content(SB.ToString()); //With AjaxForm
			} else
				return Content("INVALID DATA!");
		}
		
	}
}