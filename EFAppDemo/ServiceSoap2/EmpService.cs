﻿using AutoMapper;
using BusinessLogic;
using ServiceSoap2.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceSoap2 {
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	public class EmpService : IEmpService {
		ILogic logic;
		IMapper mapper;
		public EmpService() {
			logic = new RealLogic();
			mapper = DTO.AutoMapperConfig.GetMapper();

		}
		public List<Average> GetWorkerAverages() {
			IQueryable<BusinessLogic.GetAveragesResult> list = logic.GetAverages();
			return mapper.Map<IQueryable<BusinessLogic.GetAveragesResult>, List<DTO.Average>>(list);
		}

		public Emp GetWorkerById(int id) {
			Data.EMP worker = logic.GetOneWorker(id);
			return mapper.Map<Data.EMP, DTO.Emp>(worker);
		}
	}
}
