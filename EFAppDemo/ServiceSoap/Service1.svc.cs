﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServiceSoap.DTO;
using BusinessLogic;
using AutoMapper;

namespace ServiceSoap {
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
	public class EmpService : IEmpService {
		ILogic logic;
		IMapper mapper;
		public EmpService() {
			logic = new RealLogic();

		}
		public List<Average> GetWorkerAverages() {
			IQueryable<BusinessLogic.GetAveragesResult> list = logic.GetAverages();
			return mapper.Map<IQueryable<BusinessLogic.GetAveragesResult>, List<DTO.Average>>(list);
		}

		public Emp GetWorkerById(int id) {
			Data.EMP worker = logic.GetOneWorker(id);
			return mapper.Map<Data.EMP, DTO.Emp>(worker);
		}
	}
}
