﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebLayer.Controllers
{
	public class Storage {
		public string SomeProperty { get; set; }
	}
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
			ViewData["SomeKey"] = "This Comes from viewData";
			ViewBag.Anything = "This comes from viewBag";
			ViewBag.Something = new Storage() { SomeProperty = "This comes from a nested class prop" };
            return View();
        }
		public ActionResult Chart(int? id,string name) {
			ILogic logic = new RealLogic();
			var q = logic.GetWorkers();

			if(id.HasValue) {
				q = q.Where(x => x.EMPNO == id);
			}
			if(name != null) {
				q = q.Where(x => x.ENAME.ToLower().Contains(name.ToLower()));
			}
			var data = q.Select(x => x ).ToList();
			ViewBag.Names = data.Select(x => x.ENAME).ToArray();
			ViewBag.Values = data.Select(x => x.SAL).ToArray();
			return View();
		}
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}