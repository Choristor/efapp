﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceSoap2.DTO {
	public class AutoMapperConfig {
		//Objektumok átalakításához használatos osztály
		public static IMapper GetMapper() {
			var config = new MapperConfiguration(cfg => {
				cfg.CreateMap<Data.EMP, DTO.Emp>();//ez csak egy irányú
				cfg.CreateMap<BusinessLogic.GetAveragesResult, DTO.Average>()
					.ForMember(dest => dest.AvgSal,opt => opt.MapFrom(src => src.Avg));
			});
			return config.CreateMapper();
			
		}
	}
}