﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos {
	abstract public class EFRepository<TEntity> : IRepository<TEntity> where
		TEntity : class {
		protected DbContext context;
		public EFRepository(DbContext newctx) {
			context = newctx;
		}
		public abstract TEntity GetById(int id); //Nem oldaható meg táblafüggetlenül

		public void Delete(int id) {
			TEntity oldentity = GetById(id);
			if(oldentity == null)
				throw new ArgumentException("NO DATA");
			Delete(oldentity);
		}

		public void Delete(TEntity oldentity) {
			context.Set<TEntity>().Remove(oldentity);
			//context.Entry<TEntity>(oldentity).State = EntityState.Deleted;//A felsö ezt automatikusan végrehajtja
			context.SaveChanges();//az adatbázison lévö utasítások lementése(amíg ez nem fut le addig nem frissül az adatbázis
		}

		public void Dispose() {
			context.Dispose();
		}
		

		public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition) {
			return GetAll().Where(condition);//elöször lekérdez mindent és tovább szüri a feltétellel
		}

		public IQueryable<TEntity> GetAll() {
			return context.Set<TEntity>();
		}

		

		public void Insert(TEntity newentity) {
			context.Set<TEntity>().Add(newentity);
			context.SaveChanges();
		}
	}
}
