﻿using Data;
using Moq;
using NUnit.Framework;
using Repository;
using Repository.DeptRepos;
using Repository.EmpRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Test
{
	/*
	 * NuGet: nUnit, nUnit3TestAdapter, Moq
	 */
	 [TestFixture]
    public class BLTest
    {
		[Test]
		public void TestGetAverages() {
			//ARRANGE
			EmpDeptEntities ED = new EmpDeptEntities();
			EmpEFRepository er = new EmpEFRepository(ED);
			DeptEFRepository dr = new DeptEFRepository(ED);
			MyRepository repo = new MyRepository(er, dr);
			ILogic logic = new RealLogic(repo);

			ILogic fake = new FakeLogic();
			//ACT
			var actual = logic.GetAverages();
			var expected = fake.GetAverages();
			//ASSERT
			CollectionAssert.AreEquivalent(actual, expected);
		}
		[Test]
		public void Test_AccessesRepo() {
			//Arrange
			Mock<IEmpRepository> mockedRepo = new Mock<IEmpRepository>();

			mockedRepo.Setup(x => x.GetById(123)).Returns(new EMP() { EMPNO = 123, ENAME = "BÉLA" });
			mockedRepo.Setup(x => x.GetAll()).Returns(new List<EMP>().AsQueryable());

			Mock<IDeptRepository> deptRepo = new Mock<IDeptRepository>();
			deptRepo.Setup(x => x.GetAll()).Returns(new List<DEPT>().AsQueryable());
			MyRepository repo = new MyRepository(mockedRepo.Object, deptRepo.Object);

			ILogic logic = new RealLogic(repo);
			//ACT
			var result = logic.GetDepartments();
			//ASSERT
			Assert.That(result.Count(), Is.EqualTo(0));
			deptRepo.Verify(x => x.GetAll(), Times.Once);
		}
    }
}
