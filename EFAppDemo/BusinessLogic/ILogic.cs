﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic {
	public interface ILogic {
		IQueryable<GetAveragesResult> GetAverages();
		IQueryable<EMP> GetWorkers();
		IQueryable<DEPT> GetDepartments();

		EMP GetOneWorker(int id);
		void AddDept(DEPT newdept);

		void DelDept(int id);

		void ModifyDept(int id, string name, string loc);

		DEPT GetDept(int id);
		int GetNextDeptno();
	}
}
