﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using BusinessLogic;

namespace WebLayer.Hubs {
	public class DeptHub : Hub {
		public void DelDept(int id) {
			new RealLogic().DelDept(id);
			Clients.All.Refresh();
			
		}
		public void AddDept(DTO.Dept d) {
			Data.DEPT realDept = DTO.AutoMapperConfig.GetMapper().Map<DTO.Dept, Data.DEPT>(d);
			new RealLogic().AddDept(realDept);
			Clients.All.DeptAdded(d);
		}
	}
}