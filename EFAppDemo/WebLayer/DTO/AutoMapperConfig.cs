﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.DTO {
	public class AutoMapperConfig {
		//Objektumok átalakításához használatos osztály
		public static IMapper GetMapper() {
			var config = new MapperConfiguration(cfg => {
				cfg.CreateMap<Data.DEPT, DTO.Dept>().ReverseMap();
				cfg.CreateMap<Data.EMP, DTO.Emp>();//ez csak egy irányú
				cfg.CreateMap<BusinessLogic.GetAveragesResult, DTO.Averages>()
					.ForMember(dest => dest.AvgSal,opt => opt.MapFrom(src => src.Avg));
				cfg.CreateMap<Data.DEPT, Models.DeptModel>().ReverseMap();
			});
			return config.CreateMapper();
			
		}
	}
}