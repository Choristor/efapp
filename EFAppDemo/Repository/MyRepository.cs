﻿using Repository.DeptRepos;
using Repository.EmpRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository {
	public class MyRepository {
		
		public IEmpRepository empRepo { get; private set; }
		public IDeptRepository deptRepo { get; private set; }
		//DEPENDENCY INJECTION
		public MyRepository(IEmpRepository er, IDeptRepository dr) {
			empRepo = er;
			deptRepo = dr;
		}
	}
}
