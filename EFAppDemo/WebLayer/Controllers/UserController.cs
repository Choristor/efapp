﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebLayer.Classes;

namespace WebLayer.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Login() {
			Session["UserID"] = 42;
			return RedirectToAction("Index");
		}
		public ActionResult Logout() {
			Session.Abandon();
			return RedirectToAction("Index");

		}
		[MyAuthorizeFilter]
		public ActionResult Secret() {
			return Content("The Bitch is in");
		}
	}
}