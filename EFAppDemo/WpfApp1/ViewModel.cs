﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1 {
	class ViewModel :INotifyPropertyChanged {
		double income;
		double taxPct;
		double prepaid;
		string result;
		private bool mustPay;

		public bool MustPay {
			get { return mustPay; }
			set {
				mustPay = value;
				OnPropertyChanged();
			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		void OnPropertyChanged([CallerMemberName]string name = "") {
			var handler = PropertyChanged;
			if(handler != null) {
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

		public double Income {
			get => income;
			set {
				income = value;
				OnPropertyChanged();
			}
		}
		public double TaxPct { get => taxPct; set {
				taxPct = value;
				OnPropertyChanged();
			} }
		public double Prepaid { get => prepaid; set { prepaid = value;
				OnPropertyChanged();
			} }
		public string Result { get => result; set { result = value;
				OnPropertyChanged();
			} }



	}
}
