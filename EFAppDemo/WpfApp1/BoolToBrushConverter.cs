﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace WpfApp1 {
	class BoolToBrushConverter : IValueConverter {
		/// <summary>
		/// VM => UI
		/// </summary>
		/// <param name="value">VM property</param>
		/// <param name="targetType">UI property type</param>
		/// <param name="parameter">XAML ConverterParameter</param>
		/// <param name="culture">Thread culture</param>
		/// <returns>UI value [BRUSH]</returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			bool flag = (bool)value;
			return flag ? Brushes.Green : Brushes.Red;
		}
		/// <summary>
		/// UI => VM
		/// </summary>
		/// <param name="value">UI property</param>
		/// <param name="targetType">VM property type</param>
		/// <param name="parameter">XAML ConverterParameter</param>
		/// <param name="culture">Thread culture</param>
		/// <returns>VM value</returns>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
