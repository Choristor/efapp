﻿using AutoMapper;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebLayer.DTO;

namespace WebLayer.Controllers {
	public class TestController : ApiController {
		ILogic logic;
		IMapper mapper;
		public TestController() {
			logic = new RealLogic();
			mapper = DTO.AutoMapperConfig.GetMapper();
		}
		//  /api/test/worker/7788
		[HttpGet]
		[ActionName("worker")]
		public DTO.Emp GetSingleWorker(int id) {
			Data.EMP worker = logic.GetOneWorker(id);
			return mapper.Map<Data.EMP, DTO.Emp>(worker);
		}
		[HttpGet]
		[ActionName("averages")]
		public List<DTO.Averages> GetAverages() {
			IQueryable<BusinessLogic.GetAveragesResult> list = logic.GetAverages();
			return mapper.Map<IQueryable<BusinessLogic.GetAveragesResult>, List<DTO.Averages>>(list);
		}
		// api/test/depts
		[HttpGet]
		[ActionName("depts")]
		public List<DTO.Dept> GetDepts() {
			IQueryable<Data.DEPT> list = logic.GetDepartments();
			return mapper.Map<IQueryable<Data.DEPT>, List<DTO.Dept>>(list);
		}
		// /api/test/add
		[HttpPost]
		[ActionName("add")]
		public List<DTO.Dept> AddDept(DTO.Dept d) {
			Data.DEPT realDept = mapper.Map<Dept, Data.DEPT>(d);
			logic.AddDept(realDept);
			IQueryable<Data.DEPT> list = logic.GetDepartments();
			return mapper.Map<IQueryable<Data.DEPT>, List<DTO.Dept>>(list);
		}

		
		// GET api/<controller>
		//public IEnumerable<string> Get() {
		//	return new string[] { "value1", "value2" };
		//}

		//// GET api/<controller>/5
		//public string Get(int id) {
		//	return "value";
		//}

		//// POST api/<controller>
		//public void Post([FromBody]string value) {
		//}

		//// PUT api/<controller>/5
		//public void Put(int id, [FromBody]string value) {
		//}

		//// DELETE api/<controller>/5
		//public void Delete(int id) {
		//}
	}
}