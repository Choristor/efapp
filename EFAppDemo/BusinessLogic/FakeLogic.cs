﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository;
using Repository.EmpRepos;
using Repository.DeptRepos;

namespace BusinessLogic {
	public class FakeLogic : ILogic {
		MyRepository Repo;
		public FakeLogic() {
			EmpListRepository er = new EmpListRepository();
			DeptListRepository dr = new DeptListRepository();
			Repo = new MyRepository(er, dr);
		}
		public IQueryable<GetAveragesResult> GetAverages() {
			List<GetAveragesResult> output = new List<GetAveragesResult>();
			output.Add(new GetAveragesResult() {
				Job = "ANALYST", Avg = 3000
			});
			output.Add(new GetAveragesResult() {
				Job = "CLERK",
				Avg = 1037.5M
			});
			output.Add(new GetAveragesResult() {
				Job = "MANAGER",
				Avg = 2758.33333M
			});
			output.Add(new GetAveragesResult() {
				Job = "PRESIDENT",
				Avg = 5000
			});
			output.Add(new GetAveragesResult() {
				Job = "SALESMAN",
				Avg = 1400
			});
			return output.AsQueryable();

		}

		public IQueryable<DEPT> GetDepartments() {
			return Repo.deptRepo.GetAll();
		}

		public IQueryable<EMP> GetWorkers() {
			return Repo.empRepo.GetAll();
		}
		public EMP GetOneWorker(int id) {
			return Repo.empRepo.GetById(id);
		}

		public void AddDept(DEPT newdept) {
			Repo.deptRepo.Insert(newdept);
		}
		public void DelDept(int id) {
			Repo.deptRepo.Delete(id);
		}
		public void ModifyDept(int id, string name, string loc) {
			Repo.deptRepo.Modify(id, name, loc);
		}

		public DEPT GetDept(int id) {
			return Repo.deptRepo.GetById(id);
		}

		public int GetNextDeptno() {
			return Repo.deptRepo.GetAll().Max(x => (int)x.DEPTNO) + 10;
		}
	}
}
